package com.polynomdivision.openjboard.keyboard;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.Log;
import android.view.View;

import com.polynomdivision.openjboard.R;
import com.polynomdivision.openjboard.keyboard.keys.FlickKey;
import com.polynomdivision.openjboard.keyboard.keys.Keys;

public class InputMethod extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    // Keep the Views for communicating with them later on
    private FlickKeyboardView mKeyboardView;
    private Keyboard mKeyboard;
    // private CandidateView mCandidateView;

    // Provide GBoard-like input selection
    private int mCursorStart = -1;
    private int mCursorEnd = -1;
    private int maxCursorEnd = -1;

    @Override
    public View onCreateInputView() {
        mKeyboardView = (FlickKeyboardView) getLayoutInflater().inflate(R.layout.keyboard,null);
        mKeyboard = new Keyboard(this, R.xml.twelve_btn_flick);
        mKeyboardView.setKeyboard(mKeyboard);
        mKeyboardView.setOnKeyboardActionListener(this);

        return mKeyboardView;
    }

    @Override
    public void onPress(int primaryCode) {
        // TODO: Vibrate
    }

    @Override
    public void onRelease(int primaryCode) {
        if (Keys.isKanaCharacter(primaryCode)) {
            FlickKey fKey = FlickKey.fromKeycode(primaryCode);
            Log.d("InputMethod", "onRelease: " + fKey.fromDirection(mKeyboardView.getLastFlickDirection()));
        }
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
