package com.polynomdivision.openjboard.keyboard.keys;

public class Keys {
    public static int KANA_MODIFIER_KEYCODE = -195;

    public static boolean isKanaCharacter(int keycode) {
        return keycode <= -1100 && keycode >= -1110;
    }

    public static boolean isUtilityKey(int keycode) {
        return keycode <= -190 && keycode >= -196;
    }

    public static boolean isModifier(int keycode) {
        return keycode == KANA_MODIFIER_KEYCODE;
    }
}
