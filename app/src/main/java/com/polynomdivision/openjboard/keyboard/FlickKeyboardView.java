package com.polynomdivision.openjboard.keyboard;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.polynomdivision.openjboard.R;
import com.polynomdivision.openjboard.keyboard.keys.FlickKey;
import com.polynomdivision.openjboard.keyboard.keys.KeyDetector;
import com.polynomdivision.openjboard.keyboard.keys.Keys;
import com.polynomdivision.openjboard.keyboard.keys.FlickDirection;

public class FlickKeyboardView extends KeyboardView {
    private Paint mPaint = new Paint();

    // Is a button on the keyboard pressed?
    private boolean mIsPressed = false;
    // The coordinates the touch event begun at
    private float mDownPosX = 0f;
    private float mDownPosY = 0f;
    // The key code which was hit on the initial touch downn
    private int mLastHitKeyCode = -1;
    //private int mLastHitKeyIndex = 0;
    private Keyboard.Key mLastHitKey = null;
    // The last direction the key was flicked to
    private FlickDirection mLastHitKeyDirection = FlickDirection.DIRECTION_MIDDLE;
    // For access by the input method manager
    private FlickDirection mTouchUpDirection = FlickDirection.DIRECTION_MIDDLE;
    // Are we in the middle of writing something
    public boolean mShowKanaButton = false;

    public FlickKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlickKeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FlickDirection getLastFlickDirection() {
        return mTouchUpDirection;
    }

    /**
     * Calculates the distance that the finger was moved from (mDownPosX|mDownPosY)
     * */
    public double getMovedDistance(MotionEvent e) {
        return Math.sqrt(Math.pow(mDownPosX - e.getX(), 2) + Math.pow(mDownPosY - e.getY(), 2));
    }

    /**
     * Tells the view to show either the globe or the kana modifier icon
     *
     * Args:
     * @editing: True if the IME is inputting a word. False otherwise.
     * */
    public void setEditState(boolean editing) {
        mShowKanaButton = editing;
    }

    public boolean onTouchEvent(MotionEvent e) {
        // We don't want to pass on the ACTION_MOVE event to the super class as this would mess
        // up the initial key selection (It is possible to move your finger around on the keyboard
        // while android updates the selected key. This, however, we don't want)
        if (e.getAction() != MotionEvent.ACTION_MOVE)
            super.onTouchEvent(e);

        switch(e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // Set all variables that we need to calculate the "flick"
                mIsPressed = true;
                mDownPosX = e.getX();
                mDownPosY = e.getY();
                Key key = KeyDetector.detectHitKey(getKeyboard(), e.getX(), e.getY());
                mLastHitKeyCode = key.codes[0];
                mLastHitKey = key;

                // Force a redraw
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                // Figure out the direction
                double distance = getMovedDistance(e);

                // BUG: Android does not give us any Y coordinates when we leave the
                //      KeyboardView. This means that, unless we leave the View with
                //      distance > 108, the upper Kana cannot be entered.
                // FIX: To "fix" this bug, we just treat this - the case that e.getY() returns
                //      0f - as a special corner case!
                if (distance <= 108 && e.getY() != 0) {
                    if (mLastHitKeyDirection != FlickDirection.DIRECTION_MIDDLE) {
                        mLastHitKeyDirection = FlickDirection.DIRECTION_MIDDLE;
                        mTouchUpDirection = FlickDirection.DIRECTION_MIDDLE;

                        invalidate();
                    }
                    return true;
                }

                // In case e.getY() returns 0f, we just assume that we are much farther above the
                // keyboard than we actually are. This allows us to input the upper Kana, even if
                // Android does not allow us.
                FlickDirection dir;
                if (e.getY() == 0f) {
                    dir = FlickDirection.fromCoordinates(mDownPosX, mDownPosY, e.getX(), -108f);
                } else {
                    dir = FlickDirection.fromCoordinates(mDownPosX, mDownPosY, e.getX(), e.getY());
                }

                if (mLastHitKeyDirection != dir) {
                    mLastHitKeyDirection = dir;
                    mTouchUpDirection = dir;
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                mIsPressed = false;
                mTouchUpDirection = mLastHitKeyDirection;

                // NOTE: This has to be done, so we don't accidentally render a flick indicator
                //       on the next redraw.
                mLastHitKeyDirection = FlickDirection.DIRECTION_MIDDLE;
                invalidate();
                break;
        }

        return true;
    }

    @Override
    public void onDraw(Canvas c) {
        // Setup the canvas
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(Constants.KeyboardButtonTextSize);
        mPaint.setColor(Color.parseColor("#ffffff"));

        // Draw the keys
        for(Key key : getKeyboard().getKeys())
            drawKey(key, c);
    }

    private void drawIcon(Canvas c, Drawable icon, int x, int y, int width, int height) {
        c.translate(x, y);
        icon.setBounds(0, 0, width, height);
        icon.draw(c);
        c.translate(-x, -y);
    }

    /**
     * Draws the flick direction indicator on the canvas
     *
     * Args:
     * @key: The key which is being "flicked"
     * @c: The canvas to draw on
     * */
    public void drawKeyDirection(Key key, Canvas c) {
        Path p = new Path();

        switch(mLastHitKeyDirection) {
            case DIRECTION_UP:
                if (key.codes[0] == Keys.KANA_MODIFIER_KEYCODE)
                    break;

                p.moveTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);
                p.lineTo(key.x, key.y);
                p.lineTo(key.x + key.width, key.y);
                p.lineTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);;
                p.close();
                break;
            case DIRECTION_DOWN:
                if (key.codes[0] == Keys.KANA_MODIFIER_KEYCODE)
                    break;

                p.moveTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);
                p.lineTo(key.x, key.y + key.height);
                p.lineTo(key.x + key.width, key.y + key.height);
                p.lineTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);;
                p.close();
                break;
            case DIRECTION_LEFT:
                p.moveTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);
                p.lineTo(key.x, key.y);
                p.lineTo(key.x, key.y + key.height);
                p.lineTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);;
                p.close();
                break;
            case DIRECTION_RIGHT:
                p.moveTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);
                p.lineTo(key.x + key.width, key.y);
                p.lineTo(key.x + key.width, key.y + key.height);
                p.lineTo(key.x + key.width * 0.5f, key.y + key.height * 0.5f);;
                p.close();
                break;
        }

        mPaint.setColor(Color.parseColor("#455a64"));
        c.drawPath(p, mPaint);
        mPaint.setColor(Color.parseColor("#ffffff"));
    }

    public void drawKey(Key key, Canvas c) {
        // Highlight the currently pressed key
        // TODO: Add this with a weight to the background (Weeb picture int the background must be
        // TODO: visible)
        if (key.codes[0] == mLastHitKeyCode && mIsPressed) {
            mPaint.setColor(Color.parseColor("#78909c"));
            c.drawRect(key.x,
                    key.y,
                    key.x + key.width,
                    key.y + key.height,
                    mPaint);
            // NOTE: I forgot this in the last revision. This led to other keys turning
            // blue.
            mPaint.setColor(Color.parseColor("#ffffff"));
        }

        if (Keys.isKanaCharacter(key.codes[0])) {
            if (key.codes[0] == mLastHitKeyCode && mIsPressed)
                drawKeyDirection(key, c);

            mPaint.setTextSize(Constants.KeyboardButtonTextSize);
            c.drawText(key.label.toString(),
                    key.x + key.width * 0.5f,
                    key.y + key.height * 0.5f,
                    mPaint);

            mPaint.setTextSize(Constants.KeyboardButtonTextSizeSmall);
            FlickKey fKey = FlickKey.fromKeycode(key.codes[0]);
            c.drawText(fKey.mTop,
                    key.x + key.width * 0.5f,
                    key.y+ key.height * 0.5f - (Constants.KeyboardButtonTextSizeSmall + Constants.KeyboardButtonKanaSpacing),
                    mPaint);
            c.drawText(fKey.mLeft,
                    key.x + key.width * 0.5f - (Constants.KeyboardButtonTextSizeSmall + Constants.KeyboardButtonKanaSpacing),
                    key.y+ key.height * 0.5f,
                    mPaint);
            c.drawText(fKey.mBottom,
                    key.x + key.width * 0.5f,
                    key.y+ key.height * 0.5f + (Constants.KeyboardButtonTextSizeSmall + Constants.KeyboardButtonKanaSpacing),
                    mPaint);
            c.drawText(fKey.mRight,
                    key.x + key.width * 0.5f + (Constants.KeyboardButtonTextSizeSmall + Constants.KeyboardButtonKanaSpacing),
                    key.y+ key.height * 0.5f,
                    mPaint);
            mPaint.setTextSize(Constants.KeyboardButtonTextSize);
        } else if (Keys.isModifier(key.codes[0])) {
            if (mLastHitKeyCode == Keys.KANA_MODIFIER_KEYCODE && mShowKanaButton && mIsPressed)
                drawKeyDirection(key, c);

            // Handle the 大<->小 Key
            // The icon is context sensitive, which means that we show the globe when we're
            // not editing text and show the modifier icon when we do.
            Drawable icon;
            if (mShowKanaButton)
                icon = ContextCompat.getDrawable(getContext(), R.drawable.ic_kana_modifier);
            else
                icon = ContextCompat.getDrawable(getContext(), R.drawable.ic_globe);

            int iconHeight = icon.getIntrinsicHeight();
            int iconWidth = icon.getIntrinsicWidth();
            int iconY = (key.height - iconHeight) / 2;
            int iconX = (key.width - iconWidth) / 2;
            drawIcon(c, icon, key.x + iconX, key.y + iconY, iconWidth, iconHeight);
        } else if (Keys.isUtilityKey(key.codes[0])) {
            Drawable icon = key.icon;
            int iconHeight = icon.getIntrinsicHeight();
            int iconWidth = icon.getIntrinsicWidth();
            int iconY = (key.height - iconHeight) / 2;
            int iconX = (key.width - iconWidth) / 2;
            drawIcon(c, icon, key.x + iconX, key.y + iconY, iconWidth, iconHeight);
        } else {
            // Other keys
            if (TextUtils.isEmpty(key.label))
                return;

            c.drawText(key.label.toString(),
                    key.x + key.width * 0.5f,
                    key.y + key.height * 0.5f,
                    mPaint);
        }
    }
}
