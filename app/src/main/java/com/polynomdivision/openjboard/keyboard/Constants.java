package com.polynomdivision.openjboard.keyboard;

public class Constants {
    // Used for drawing the middle character
    public static int KeyboardButtonTextSize = 60;

    // Used for drawing the outer characters
    public static int KeyboardButtonTextSizeSmall = 45;

    // Used for drawing the outer characters
    public static int KeyboardButtonKanaSpacing = 12;
}
