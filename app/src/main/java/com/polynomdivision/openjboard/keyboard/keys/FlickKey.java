package com.polynomdivision.openjboard.keyboard.keys;

public class FlickKey {
    public String mTop, mBottom, mLeft, mRight, mMiddle;

    public FlickKey(String middle, String top, String left, String bottom, String right) {
        mTop = top;
        mBottom = bottom;
        mLeft = left;
        mRight = right;
        mMiddle = middle;
    }

    public static FlickKey fromLabel(String label) {
        switch (label) {
            case "あ":
                return new FlickKey("あ", "う", "い", "お", "え");
            case "か":
                return new FlickKey("か", "く", "き", "こ", "け");
            case "さ":
                return new FlickKey("さ", "す", "し", "そ", "せ");
            case "た":
                return new FlickKey("た", "つ", "ち", "と", "て");
            case "な":
                return new FlickKey("な", "ぬ", "に", "の", "ね");
            case "は":
                return new FlickKey("は", "ふ", "ひ", "ほ", "へ");
            case "ま":
                return new FlickKey("ま", "む", "み", "も", "め");
            case "や":
                return new FlickKey("や", "ゆ", "（", "よ", "）");
            case "ら":
                return new FlickKey("ら", "る", "り", "ろ", "れ");
            case "わ":
                return new FlickKey("わ", "ん", "を", "〜", "ー");
            case "、":
                return new FlickKey("、", "？", "。", "…", "！");
        }

        return new FlickKey("?", "?", "?", "?", "?");
    }

    public static FlickKey fromKeycode(int i) {
        switch (i) {
            case -1100:
                return new FlickKey("あ", "う", "い", "お", "え");
            case -1101:
                return new FlickKey("か", "く", "き", "こ", "け");
            case -1102:
                return new FlickKey("さ", "す", "し", "そ", "せ");
            case -1103:
                return new FlickKey("た", "つ", "ち", "と", "て");
            case -1104:
                return new FlickKey("な", "ぬ", "に", "の", "ね");
            case -1105:
                return new FlickKey("は", "ふ", "ひ", "ほ", "へ");
            case -1106:
                return new FlickKey("ま", "む", "み", "も", "め");
            case -1107:
                return new FlickKey("や", "ゆ", "（", "よ", "）");
            case -1108:
                return new FlickKey("ら", "る", "り", "ろ", "れ");
            case -1109:
                return new FlickKey("わ", "ん", "を", "〜", "ー");
            case -1110:
                return new FlickKey("、", "？", "。", "…", "！");
        }

        return new FlickKey("?", "?", "?", "?", "?");
    }

    public String fromDirection(FlickDirection d) {
        switch(d) {
            case DIRECTION_LEFT:
                return mLeft;
            case DIRECTION_RIGHT:
                return mRight;
            case DIRECTION_UP:
                return mTop;
            case DIRECTION_DOWN:
                return mBottom;
            case DIRECTION_MIDDLE:
                return mMiddle;
        }

        // This should not happen
        return "";
    }

    /*public void render(int small, int big, int kanaSpacing, Key key, Canvas canvas, Paint paint) {
        paint.setTextSize(big);
        canvas.drawText(mMiddle,
                key.x + key.width * 0.5f,
                key.y+ key.height * 0.5f,
                paint);

        paint.setTextSize(small);
        canvas.drawText(mTop,
                key.x + key.width * 0.5f,
                key.y+ key.height * 0.5f - (small + kanaSpacing),
                paint);
        canvas.drawText(mLeft,
                key.x + key.width * 0.5f - (small + kanaSpacing),
                key.y+ key.height * 0.5f,
                paint);
        canvas.drawText(mBottom,
                key.x + key.width * 0.5f,
                key.y+ key.height * 0.5f + (small + kanaSpacing),
                paint);
        canvas.drawText(mRight,
                key.x + key.width * 0.5f + (small + kanaSpacing),
                key.y+ key.height * 0.5f,
                paint);
        paint.setTextSize(big);
    }*/
}
