package com.polynomdivision.openjboard.keyboard.keys;

public enum FlickDirection {
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    // Pretty much a none value
    DIRECTION_MIDDLE;

    /*
     * Figure out the direction that a vector with an angle of @theta would
     * point to.
     * */
    public static FlickDirection fromAngle(double theta) {
        if (theta >= 45 && theta <= 135) {
            return DIRECTION_UP;
        } else if ((theta >= 0 && theta <= 45)
                || (theta >= 315 && theta <= 360)) {
            return DIRECTION_RIGHT;
        } else if (theta >= 225 && theta <= 315) {
            return DIRECTION_DOWN;
        } else {
            return DIRECTION_LEFT;
        }
    }

    public static FlickDirection fromCoordinates(float x1, float y1, float x2, float y2) {
        double rad = Math.atan2(y1 - y2, x2 - x1) + Math.PI;
        double theta = (rad * 180 / Math.PI + 180) % 360;

        return FlickDirection.fromAngle(theta);
    }
}
