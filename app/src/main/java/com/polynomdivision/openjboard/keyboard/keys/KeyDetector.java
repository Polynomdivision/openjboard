package com.polynomdivision.openjboard.keyboard.keys;

import android.inputmethodservice.Keyboard;

public class KeyDetector {
    /**
     * Tries to find the key that was hit when a touch event occurred at
     * (x, y).
     *
     * Args:
     *  @keyboard: The [Keyboard] object which describes the keyboard
     *  @x, y: The coordinates of the touch event
     *
     * Returns:
     *  A [Key] object, when a key could be found. Null otherwise.
     * */
    public static Keyboard.Key detectHitKey(Keyboard keyboard, float x, float y) {
        Keyboard.Key key = null;
        for (int i : keyboard.getNearestKeys((int) x, (int) y)) {
            key = keyboard.getKeys().get(i);
            if (!key.isInside((int) x, (int) y))
                continue;

            break;
        }

        return key;
    }
}
