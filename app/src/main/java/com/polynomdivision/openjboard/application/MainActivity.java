package com.polynomdivision.openjboard.application;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.polynomdivision.openjboard.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void selectIME(View view) {
        ((InputMethodManager) this.getApplicationContext().getSystemService(
                Context.INPUT_METHOD_SERVICE)).showInputMethodPicker();
    }
}
